select 
day_id
,visit_start_time1 
,visit_start_dt
,fullVisitorId
,device_id 
,ssoid
,app_name
,app_platform 
,ua_id
,screen_name
,app_os
,shelf_in_app
,event_cate
,event_action
,event_label
,event1 
,event2 
,event3 
,event4 
,event5 
,event6 
,event7
,event8
,UTM_referral
,UTM_campaign
,UTM_source
,UTM_medium
,UTM_keyword
from (
SELECT
day_id
,visit_start_time1 
,visit_start_dt
,fullVisitorId
,device_id 
,ssoid
,app_name
,app_platform 
,ua_id
,screen_name
,case
when ua_id in ('UA-86733131-1', 'UA-86733131-16', 'UA-86733131-4', 'UA-86733131-11', 'UA-86733131-19') then 'Web'
when ua_id in ('UA-86733131-2', 'UA-86733131-15', 'UA-86733131-5', 'UA-86733131-12', 'UA-86733131-18') then 'iOS'
when ua_id in ('UA-86733131-3', 'UA-86733131-14', 'UA-86733131-6', 'UA-86733131-13', 'UA-86733131-17')  then 'Android'
else 'Others'
end  as app_os
,case 
when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  
and regexp_match(upper(screen_name),'SPORT') 
then 'Sport' 

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
and regexp_match(upper(screen_name),'SOCCER|TSS|PRE-GAME|EXCLUSIVE|RECENT MATCHES')
and not regexp_match(upper(screen_name),'MOVIE|SERIES')
then 'Soccer' 

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'MOVIE|SERIES|หนัง')
then 'Movie'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'TV')
then 'TV'

when (ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
and regexp_match(upper(screen_name),'MUSIC|SONG|PLAYLIST|HITS|ARTIST|LISTENING|ดนตรี|อัลบั้ม|เพลง|MINI PLAYER'))
then 'Music' 

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'GAME')
and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
then 'Game' 

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'TRAVEL')  
then 'Travel'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'FOOD')  
then 'Food'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'WOMEN')  
then 'Women'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'ENTERTAINMENT|DARA|NEWS')  
then 'Entertainment' 

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'MEN')
and not regexp_match(upper(screen_name),'WOMAN')
and not regexp_match(upper(screen_name),'ENTERTAINMENT')
and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
then 'Men' 

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'LONGDO')
then 'Longdo'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'KIDS')
then 'Kids'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'TRUECLOUD')
then 'TrueCloud'

WHEN ua_id in ('UA-86733131-1', 'UA-86733131-2', 'UA-86733131-3')
and regexp_match(screen_name, 'Ayothaya')
THEN 'Ayothaya'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
and regexp_match(upper(screen_name),'PAYMENT')
then 'Payment'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')
and regexp_match(upper(screen_name), 'BANNER|ADS') 
and not regexp_match(upper(screen_name), 'MEMBER')
then 'Ads'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
and (regexp_match(upper(screen_name) ,'LOGIN|MY ACCOUNT|SETTINGS|PROFILE|REGISTER') 
or regexp_match(upper(screen_name) ,'SIGNIN|FORGET|ACCOUNTS|SIGNUP'))
then 'User settings'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'CLIP')
then 'Clip'  

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'DISCOVER')
then 'Discover'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')
and regexp_match(upper(screen_name), 'UNIVERSE') 
and not regexp_match(upper(screen_name), 'WOMEN|HOME|DARA|TRUE WIFI')
then 'Universe'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name), 'TRUE WIFI')
then 'True Wifi'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')
and screen_name in ('Privilege - All','Privilege - Detail','Privilege - Detail - Article','Privilege - Dining','Privilege - Enjoy'
,'Privilege - Featured','Privilege - Shopping','Privilege Near Me - All','Privilege Near Me - Dining','Privilege Near Me - Enjoy',
'Privilege Near Me - Group' ,'Profile - TrueYou Mastercard','Privilege Near Me - Shopping','Privilege - More','Privilege - Near Me',
'Profile - TrueYou Mastercard Info','Profile - TrueYou Mastercard QRcode' )
then 'Privilege'
else 'Others'
end as shelf_in_app

,event_cate
,event_action
,event_label
,event1 
,event2 
,event3 
,event4 
,event5 
,event6 
,event7
,event8
,UTM_referral
,UTM_campaign
,UTM_source
,UTM_medium
,UTM_keyword
from
(
select
day_id
,visit_start_time1 
,visit_start_dt
,fullVisitorId
,device_id 
,ssoid
,app_name
,app_platform 
,ua_id
,screen_name
,event_cate
,event_action
,event_label
,event1 
,event2 
,event3 
,event4 
,event5 
,event6 
,event7
,event8
,UTM_referral
,UTM_campaign
,UTM_source
,UTM_medium
,UTM_keyword
FROM [true-dmp:DM_INTERNAL.tr_main_tracking] 
where NOT (REGEXP_MATCH(screen_name, 'developers') or REGEXP_MATCH(screen_name, 'localhost/') or REGEXP_MATCH(screen_name, 'dev.') )
AND (_PARTITIONTIME >= TIMESTAMP('2017-12-01') AND _PARTITIONTIME <= TIMESTAMP('2017-12-31'))
and app_name = 'TrueID'
)
)



bq rm -f Ball_dataset.tr_trueid_shelf
table_destination = Ball_dataset.tr_trueid_shelf
query="

select 
day_id
,visit_start_time1 
,visit_start_dt
,fullVisitorId
,device_id 
,ssoid
,app_name
,app_platform 
,ua_id
,screen_name
,app_os
,shelf_in_app
,event_cate
,event_action
,event_label
,event1 
,event2 
,event3 
,event4 
,event5 
,event6 
,event7
,event8
,UTM_referral
,UTM_campaign
,UTM_source
,UTM_medium
,UTM_keyword
from (
SELECT
day_id
,visit_start_time1 
,visit_start_dt
,fullVisitorId
,device_id 
,ssoid
,app_name
,app_platform 
,ua_id
,screen_name
,case
when ua_id in ('UA-86733131-1', 'UA-86733131-16', 'UA-86733131-4', 'UA-86733131-11', 'UA-86733131-19') then 'Web'
when ua_id in ('UA-86733131-2', 'UA-86733131-15', 'UA-86733131-5', 'UA-86733131-12', 'UA-86733131-18') then 'iOS'
when ua_id in ('UA-86733131-3', 'UA-86733131-14', 'UA-86733131-6', 'UA-86733131-13', 'UA-86733131-17')  then 'Android'
else 'Others'
end  as app_os
,case 
when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  
and regexp_match(upper(screen_name),'SPORT') 
then 'Sport' 

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
and regexp_match(upper(screen_name),'SOCCER|TSS|PRE-GAME|EXCLUSIVE|RECENT MATCHES')
and not regexp_match(upper(screen_name),'MOVIE|SERIES')
then 'Soccer' 

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'MOVIE|SERIES|หนัง')
then 'Movie' 

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'TV')
then 'TV' 

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
and regexp_match(upper(screen_name),'MUSIC|SONG|PLAYLIST|HITS|ARTIST|LISTENING|ดนตรี|อัลบั้ม|เพลง|MINI PLAYER')
or ua_id in ('UA-86733131-4','UA-86733131-5','UA-86733131-6') 
then 'Music' 

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'GAME')
and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
then 'Game' 

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'TRAVEL')  
then 'Travel'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'FOOD')  
then 'Food'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'WOMEN')  
then 'Women'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'ENTERTAINMENT|DARA|NEWS')  
then 'Entertainment' 

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'MEN')
and not regexp_match(upper(screen_name),'WOMAN')
and not regexp_match(upper(screen_name),'ENTERTAINMENT')
and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
then 'Men' 

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'LONGDO')
then 'Longdo'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'KIDS')
then 'Kids'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'TRUECLOUD')
then 'TrueCloud'

WHEN ua_id in ('UA-86733131-1', 'UA-86733131-2', 'UA-86733131-3')
and regexp_match(screen_name, 'Ayothaya')
THEN 'Ayothaya'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
and regexp_match(upper(screen_name),'PAYMENT')
then 'Payment'

when ua_id 
in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')
and regexp_match(upper(screen_name), 'BANNER|ADS') and not regexp_match(upper(screen_name), 'MEMBER')
then 'Ads'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
and (regexp_match(upper(screen_name) ,'LOGIN|MY ACCOUNT|SETTINGS|PROFILE|REGISTER') or regexp_match(upper(screen_name) ,'SIGNIN|FORGET|ACCOUNTS|SIGNUP'))
then 'User settings'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'CLIP')
then 'Clip'  

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name),'DISCOVER')
then 'Discover'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')
and regexp_match(upper(screen_name), 'UNIVERSE') 
and not regexp_match(upper(screen_name), 'WOMEN|HOME|DARA|TRUE WIFI')
then 'Universe'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and regexp_match(upper(screen_name), 'TRUE WIFI')
then 'True Wifi'

when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')
and screen_name in ('Privilege - All','Privilege - Detail','Privilege - Detail - Article','Privilege - Dining','Privilege - Enjoy'
,'Privilege - Featured','Privilege - Shopping','Privilege Near Me - All','Privilege Near Me - Dining','Privilege Near Me - Enjoy',
'Privilege Near Me - Group' ,'Profile - TrueYou Mastercard','Privilege Near Me - Shopping','Privilege - More','Privilege - Near Me',
'Profile - TrueYou Mastercard Info','Profile - TrueYou Mastercard QRcode' )
then 'Privilege'
else 'Others'
end as shelf_in_app

,event_cate
,event_action
,event_label
,event1 
,event2 
,event3 
,event4 
,event5 
,event6 
,event7
,event8
,UTM_referral
,UTM_campaign
,UTM_source
,UTM_medium
,UTM_keyword
from
(
select
day_id
,visit_start_time1 
,visit_start_dt
,fullVisitorId
,device_id 
,ssoid
,app_name
,app_platform 
,ua_id
,screen_name
,event_cate
,event_action
,event_label
,event1 
,event2 
,event3 
,event4 
,event5 
,event6 
,event7
,event8
,UTM_referral
,UTM_campaign
,UTM_source
,UTM_medium
,UTM_keyword
FROM [true-dmp:DM_INTERNAL.tr_main_tracking] 
where NOT (REGEXP_MATCH(screen_name, 'developers') or REGEXP_MATCH(screen_name, 'localhost/') or REGEXP_MATCH(screen_name, 'dev.') )
AND (_PARTITIONTIME >= TIMESTAMP('2017-12-01') AND _PARTITIONTIME <= TIMESTAMP('2017-12-31'))
and app_name = 'TrueID'
)
)

"
bq query --allow_large_results --replace --noflatten_results --destination_table="$table_destination" "$query"



when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')   
and event_label in ('Play TV,ev01,Thailand eSports Tournament,'
,'ev01,live,Thailand eSports Tournament,'
,'ev01,tv_live,Thailand eSports Tournament
,','ev01,Thailand eSports Tournament,')
and upper(screen_name) like'%Live%' 
and event_Action = 'Play TV' 
and event_Action is not null
and ssoid is not null
then 'E-sports'

when ua_id in ('UA-86733131-1', 'UA-86733131-2','UA-86733131-3')  
and upper(event_label) like '%SOULVEE%'
--and (upper(display_name) like "%TRUEID%" and upper(display_name) not like "%WEB%")
and event_Action is not null
and ssoid is not null
then 'Soulvee'

when ua_id in ('UA-86733131-1', 'UA-86733131-2','UA-86733131-3')
and upper(event_label) like '%ZBING%'
--and (upper(display_name) like "%TRUEID%" and upper(display_name) not like "%WEB%")
and event_Action is not null
and ssoid is not null
then 'ZBING'

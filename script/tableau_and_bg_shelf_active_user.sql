---------------------------------------------------- Daily -------------------------------------------------------------
SELECT
visit_start_dt 
,app_platform
,app_os
,shelf_name
,no_unique_user
from (
SELECT
    visit_start_dt 
    ,app_platform
    ,case
    when ua_id in ('UA-86733131-1', 'UA-86733131-16', 'UA-86733131-4', 'UA-86733131-11', 'UA-86733131-19') then 'Web'
    when ua_id in ('UA-86733131-2', 'UA-86733131-15', 'UA-86733131-5', 'UA-86733131-12', 'UA-86733131-18') then 'iOS'
    when ua_id in ('UA-86733131-3', 'UA-86733131-14', 'UA-86733131-6', 'UA-86733131-13', 'UA-86733131-17')  then 'Android'
    else 'Others'
    end  as app_os
    ,case 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  
        and regexp_match(upper(screen_name),'SPORT|SOCCER|TSS|PRE-GAME|EXCLUSIVE|RECENT MATCHES')
        and not regexp_match(upper(screen_name),'MOVIE|SERIES')
        or screen_name = ' - Article'
        then 'Sport' 
        
        when (ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and (regexp_match(upper(screen_name),'MOVIE|TV|SERIES|หนัง') 
        or regexp_match(upper(screen_name) , 'THEATER|ANIMATION|AYOTHAYA') ))
        or ua_id in ('UA-86733131-14','UA-86733131-15','UA-86733131-16') 
        then 'Movie/TV' 
        
        when (ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and regexp_match(upper(screen_name),'MUSIC|SONG|PLAYLIST|HITS|ARTIST|ดนตรี|อัลบั้ม|เพลง|MINI PLAYER') )
        or ua_id in ('UA-86733131-4','UA-86733131-5','UA-86733131-6') 
        then 'Music' 
        
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and (regexp_match(upper(screen_name),'TRAVEL|FOOD|GAME|MEN|WOMEN|ENTERTAINMENT|DARA|NEWS|LISTENING|LONGDO|KIDS|CLIP') 
        and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME'))
        then 'LifeStyle' 

        when ua_id in ('UA-86733131-1', 'UA-86733131-2', 'UA-86733131-3')
        and regexp_match(upper(screen_name),'TRUECLOUD') 
        then 'TrueCloud'
        
        when (ua_id in ('UA-86733131-13','UA-86733131-12', 'UA-86733131-11')
        and regexp_match(event_cate, 'Payment|Buy Extra') and NOT REGEXP_MATCH(event_action, 'Cancel')) 
        or (ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') and regexp_match(upper(screen_name),'PAYMENT'))
        then 'Payment'

        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')
        and regexp_match(upper(screen_name), 'BANNER|ADS') and not regexp_match(upper(screen_name), 'MEMBER')
        then 'Ads'
        
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and (regexp_match(upper(screen_name) ,'LOGIN|MY ACCOUNT|SETTINGS|PROFILE|REGISTER')
        or regexp_match(upper(screen_name) ,'SIGNIN|FORGET|ACCOUNTS|SIGNUP') )
        then 'User settings'

        when ua_id in ('UA-86733131-17','UA-86733131-18', 'UA-86733131-19') 
        or (ua_id in ('UA-86733131-1', 'UA-86733131-2', 'UA-86733131-3') 
        and screen_name in ('Privilege - All','Privilege - Detail','Privilege - Detail - Article',
        'Privilege - Dining', 'Privilege - Enjoy','Privilege - Featured','Privilege - Shopping','Privilege Near Me - All','Privilege Near Me - Dining'
        ,'Privilege Near Me - Enjoy','Privilege Near Me - Group' ,'Profile - TrueYou Mastercard','Privilege Near Me - Shopping'
        ,'Privilege - More','Privilege - Near Me','Profile - TrueYou Mastercard Info','Profile - TrueYou Mastercard QRcode')
        )

        then 'Privilege'
        else 'Others' 
    end as shelf_name

  ,exact_count_distinct(fullVisitorId) as no_unique_user
from
(
select 
    visit_start_dt 
    ,fullVisitorId
    ,screen_name
    ,app_platform
    ,event_cate
    ,event_action
    ,ua_id
    FROM [true-dmp:DM_INTERNAL.tr_main_tracking] 
    where NOT (REGEXP_MATCH(screen_name, 'developers') or REGEXP_MATCH(screen_name, 'localhost/') or REGEXP_MATCH(screen_name, 'dev.'))
    AND (_PARTITIONTIME >= TIMESTAMP(DATE_ADD('2017-10-31',-30,'DAY')) AND _PARTITIONTIME <= TIMESTAMP('2017-10-31'))
    and app_name in ('TrueMusic', 'TrueTV', 'TrueID', 'iService', 'TrueYou')
)
group by 1,2,3,4
)

------------------------------------ Tableau Daily -----------------------------------------------------

SELECT
visit_start_dt 
,app_platform
,app_os
,shelf_name
,no_unique_user
from (
SELECT
    visit_start_dt 
    ,app_platform
    ,case
    when ua_id in ('UA-86733131-1', 'UA-86733131-16', 'UA-86733131-4', 'UA-86733131-11', 'UA-86733131-19') then 'Web'
    when ua_id in ('UA-86733131-2', 'UA-86733131-15', 'UA-86733131-5', 'UA-86733131-12', 'UA-86733131-18') then 'iOS'
    when ua_id in ('UA-86733131-3', 'UA-86733131-14', 'UA-86733131-6', 'UA-86733131-13', 'UA-86733131-17')  then 'Android'
    else 'Others'
    end  as app_os
    ,case 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  
        and regexp_match(upper(screen_name),'SPORT|SOCCER|TSS|PRE-GAME|EXCLUSIVE|RECENT MATCHES')
        and not regexp_match(upper(screen_name),'MOVIE|SERIES')
        or screen_name = ' - Article'
        then 'Sport' 
        
        when (ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and (regexp_match(upper(screen_name),'MOVIE|TV|SERIES|หนัง') 
        or regexp_match(upper(screen_name) , 'THEATER|ANIMATION|AYOTHAYA') ))
        or ua_id in ('UA-86733131-14','UA-86733131-15','UA-86733131-16') 
        then 'Movie/TV' 
        
        when (ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and regexp_match(upper(screen_name),'MUSIC|SONG|PLAYLIST|HITS|ARTIST|ดนตรี|อัลบั้ม|เพลง|MINI PLAYER') )
        or ua_id in ('UA-86733131-4','UA-86733131-5','UA-86733131-6') 
        then 'Music' 
        
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and (regexp_match(upper(screen_name),'TRAVEL|FOOD|GAME|MEN|WOMEN|ENTERTAINMENT|DARA|NEWS|LISTENING|LONGDO|KIDS|CLIP') 
        and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME'))
        then 'LifeStyle' 

        when ua_id in ('UA-86733131-1', 'UA-86733131-2', 'UA-86733131-3')
        and regexp_match(upper(screen_name),'TRUECLOUD') 
        then 'TrueCloud'
        
        when (ua_id in ('UA-86733131-13','UA-86733131-12', 'UA-86733131-11')
        and regexp_match(event_cate, 'Payment|Buy Extra') and NOT REGEXP_MATCH(event_action, 'Cancel')) 
        or (ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') and regexp_match(upper(screen_name),'PAYMENT'))
        then 'Payment'

        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')
        and regexp_match(upper(screen_name), 'BANNER|ADS') and not regexp_match(upper(screen_name), 'MEMBER')
        then 'Ads'
        
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and (regexp_match(upper(screen_name) ,'LOGIN|MY ACCOUNT|SETTINGS|PROFILE|REGISTER')
        or regexp_match(upper(screen_name) ,'SIGNIN|FORGET|ACCOUNTS|SIGNUP') )
        then 'User settings'

        when ua_id in ('UA-86733131-17','UA-86733131-18', 'UA-86733131-19') 
        or (ua_id in ('UA-86733131-1', 'UA-86733131-2', 'UA-86733131-3') 
        and screen_name in ('Privilege - All','Privilege - Detail','Privilege - Detail - Article',
        'Privilege - Dining', 'Privilege - Enjoy','Privilege - Featured','Privilege - Shopping','Privilege Near Me - All','Privilege Near Me - Dining'
        ,'Privilege Near Me - Enjoy','Privilege Near Me - Group' ,'Profile - TrueYou Mastercard','Privilege Near Me - Shopping'
        ,'Privilege - More','Privilege - Near Me','Profile - TrueYou Mastercard Info','Profile - TrueYou Mastercard QRcode')
        )

        then 'Privilege'
        else 'Others' 
    end as shelf_name
  ,exact_count_distinct(fullVisitorId) as no_unique_user
from
(
select 
    visit_start_dt 
    ,fullVisitorId
    ,screen_name
    ,app_platform
    ,event_cate
    ,event_action
    ,ua_id
    FROM [true-dmp:DM_INTERNAL.tr_main_tracking] 
    where NOT (REGEXP_MATCH(screen_name, 'developers') or REGEXP_MATCH(screen_name, 'localhost/') or REGEXP_MATCH(screen_name, 'dev.'))
    AND (_PARTITIONTIME >= TIMESTAMP(DATE_ADD(<Parameters.Start Date>,-30,'DAY')) AND _PARTITIONTIME <= TIMESTAMP(<Parameters.Start Date>))
    and app_name in ('TrueMusic', 'TrueTV', 'TrueID', 'iService', 'TrueYou')
)
group by 1,2,3,4
)

------------------------------------ Histogram -----------------------------------------------------

SELECT
app_platform
,app_os
,shelf_name
,no_unique_user
FROM(
SELECT
    app_platform
    ,case
    when ua_id in ('UA-86733131-1', 'UA-86733131-16', 'UA-86733131-4', 'UA-86733131-11', 'UA-86733131-19') then 'Web'
    when ua_id in ('UA-86733131-2', 'UA-86733131-15', 'UA-86733131-5', 'UA-86733131-12', 'UA-86733131-18') then 'iOS'
    when ua_id in ('UA-86733131-3', 'UA-86733131-14', 'UA-86733131-6', 'UA-86733131-13', 'UA-86733131-17')  then 'Android'
    else 'Others'
    end  as app_os
    ,case 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  
        and regexp_match(upper(screen_name),'SPORT|SOCCER|TSS|PRE-GAME|EXCLUSIVE|RECENT MATCHES')
        and not regexp_match(upper(screen_name),'MOVIE|SERIES')
        or screen_name = ' - Article'
        then 'Sport' 
        
        when (ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and (regexp_match(upper(screen_name),'MOVIE|TV|SERIES|หนัง') 
        or regexp_match(upper(screen_name) , 'THEATER|ANIMATION|AYOTHAYA') ))
        or ua_id in ('UA-86733131-14','UA-86733131-15','UA-86733131-16') 
        then 'Movie/TV' 
        
        when (ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and regexp_match(upper(screen_name),'MUSIC|SONG|PLAYLIST|HITS|ARTIST|ดนตรี|อัลบั้ม|เพลง|MINI PLAYER') )
        or ua_id in ('UA-86733131-4','UA-86733131-5','UA-86733131-6') 
        then 'Music' 
        
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and (regexp_match(upper(screen_name),'TRAVEL|FOOD|GAME|MEN|WOMEN|ENTERTAINMENT|DARA|NEWS|LISTENING|LONGDO|KIDS|CLIP') 
        and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME'))
        then 'LifeStyle' 

        when ua_id in ('UA-86733131-1', 'UA-86733131-2', 'UA-86733131-3')
        and regexp_match(upper(screen_name),'TRUECLOUD') 
        then 'TrueCloud'
        
        when (ua_id in ('UA-86733131-13','UA-86733131-12', 'UA-86733131-11')
        and regexp_match(event_cate, 'Payment|Buy Extra') and NOT REGEXP_MATCH(event_action, 'Cancel')) 
        or (ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') and regexp_match(upper(screen_name),'PAYMENT'))
        then 'Payment'

        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')
        and regexp_match(upper(screen_name), 'BANNER|ADS') and not regexp_match(upper(screen_name), 'MEMBER')
        then 'Ads'
        
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and (regexp_match(upper(screen_name) ,'LOGIN|MY ACCOUNT|SETTINGS|PROFILE|REGISTER')
        or regexp_match(upper(screen_name) ,'SIGNIN|FORGET|ACCOUNTS|SIGNUP') )
        then 'User settings'

        when ua_id in ('UA-86733131-17','UA-86733131-18', 'UA-86733131-19') 
        or (ua_id in ('UA-86733131-1', 'UA-86733131-2', 'UA-86733131-3') 
        and screen_name in ('Privilege - All','Privilege - Detail','Privilege - Detail - Article',
        'Privilege - Dining', 'Privilege - Enjoy','Privilege - Featured','Privilege - Shopping','Privilege Near Me - All','Privilege Near Me - Dining'
        ,'Privilege Near Me - Enjoy','Privilege Near Me - Group' ,'Profile - TrueYou Mastercard','Privilege Near Me - Shopping'
        ,'Privilege - More','Privilege - Near Me','Profile - TrueYou Mastercard Info','Profile - TrueYou Mastercard QRcode')
        )

        then 'Privilege'
        else 'Others' 
    end as shelf_name
  ,exact_count_distinct(fullVisitorId) as no_unique_user
from
(
select 
    fullVisitorId
    ,screen_name
    ,app_platform
    ,event_cate
    ,event_action
    ,ua_id
    FROM [true-dmp:DM_INTERNAL.tr_main_tracking] 
    where NOT (REGEXP_MATCH(screen_name, 'developers') or REGEXP_MATCH(screen_name, 'localhost/') or REGEXP_MATCH(screen_name, 'dev.'))
    AND (_PARTITIONTIME >= TIMESTAMP(DATE_ADD('2017-10-31',-30,'DAY')) AND _PARTITIONTIME <= TIMESTAMP('2017-10-31'))
    and app_name in ('TrueMusic', 'TrueTV', 'TrueID', 'iService', 'TrueYou')
)
group by 1,2,3
)

------------------------------------ Tableau Distribution  -----------------------------------------------------

SELECT
app_platform
,app_os
,shelf_name
,no_unique_user
FROM(
SELECT
    app_platform
    ,case
    when ua_id in ('UA-86733131-1', 'UA-86733131-16', 'UA-86733131-4', 'UA-86733131-11', 'UA-86733131-19') then 'Web'
    when ua_id in ('UA-86733131-2', 'UA-86733131-15', 'UA-86733131-5', 'UA-86733131-12', 'UA-86733131-18') then 'iOS'
    when ua_id in ('UA-86733131-3', 'UA-86733131-14', 'UA-86733131-6', 'UA-86733131-13', 'UA-86733131-17')  then 'Android'
    else 'Others'
    end  as app_os
    ,case 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  
        and regexp_match(upper(screen_name),'SPORT|SOCCER|TSS|PRE-GAME|EXCLUSIVE|RECENT MATCHES')
        and not regexp_match(upper(screen_name),'MOVIE|SERIES')
        or screen_name = ' - Article'
        then 'Sport' 
        
        when (ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and (regexp_match(upper(screen_name),'MOVIE|TV|SERIES|หนัง') 
        or regexp_match(upper(screen_name) , 'THEATER|ANIMATION|AYOTHAYA') ))
        or ua_id in ('UA-86733131-14','UA-86733131-15','UA-86733131-16') 
        then 'Movie/TV' 
        
        when (ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and regexp_match(upper(screen_name),'MUSIC|SONG|PLAYLIST|HITS|ARTIST|ดนตรี|อัลบั้ม|เพลง|MINI PLAYER') )
        or ua_id in ('UA-86733131-4','UA-86733131-5','UA-86733131-6') 
        then 'Music' 
        
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and (regexp_match(upper(screen_name),'TRAVEL|FOOD|GAME|MEN|WOMEN|ENTERTAINMENT|DARA|NEWS|LISTENING|LONGDO|KIDS|CLIP') 
        and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME'))
        then 'LifeStyle' 

        when ua_id in ('UA-86733131-1', 'UA-86733131-2', 'UA-86733131-3')
        and regexp_match(upper(screen_name),'TRUECLOUD') 
        then 'TrueCloud'
        
        when (ua_id in ('UA-86733131-13','UA-86733131-12', 'UA-86733131-11')
        and regexp_match(event_cate, 'Payment|Buy Extra') and NOT REGEXP_MATCH(event_action, 'Cancel')) 
        or (ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') and regexp_match(upper(screen_name),'PAYMENT'))
        then 'Payment'

        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')
        and regexp_match(upper(screen_name), 'BANNER|ADS') and not regexp_match(upper(screen_name), 'MEMBER')
        then 'Ads'
        
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and (regexp_match(upper(screen_name) ,'LOGIN|MY ACCOUNT|SETTINGS|PROFILE|REGISTER')
        or regexp_match(upper(screen_name) ,'SIGNIN|FORGET|ACCOUNTS|SIGNUP') )
        then 'User settings'

        when ua_id in ('UA-86733131-17','UA-86733131-18', 'UA-86733131-19') 
        or (ua_id in ('UA-86733131-1', 'UA-86733131-2', 'UA-86733131-3') 
        and screen_name in ('Privilege - All','Privilege - Detail','Privilege - Detail - Article',
        'Privilege - Dining', 'Privilege - Enjoy','Privilege - Featured','Privilege - Shopping','Privilege Near Me - All','Privilege Near Me - Dining'
        ,'Privilege Near Me - Enjoy','Privilege Near Me - Group' ,'Profile - TrueYou Mastercard','Privilege Near Me - Shopping'
        ,'Privilege - More','Privilege - Near Me','Profile - TrueYou Mastercard Info','Profile - TrueYou Mastercard QRcode')
        )

        then 'Privilege'
        else 'Others' 
    end as shelf_name
  ,exact_count_distinct(fullVisitorId) as no_unique_user
from
(
select 
    fullVisitorId
    ,screen_name
    ,app_platform
    ,event_cate
    ,event_action
    ,ua_id
    FROM [true-dmp:DM_INTERNAL.tr_main_tracking] 
    where NOT (REGEXP_MATCH(screen_name, 'developers') or REGEXP_MATCH(screen_name, 'localhost/') or REGEXP_MATCH(screen_name, 'dev.'))
    AND (_PARTITIONTIME >= TIMESTAMP(DATE_ADD(<Parameters.Start Date>,-30,'DAY')) AND _PARTITIONTIME <= TIMESTAMP(<Parameters.Start Date>))
    and app_name in ('TrueMusic', 'TrueTV', 'TrueID', 'iService', 'TrueYou')
)
group by 1,2,3
)

------------------------------------- Monthly -----------------------------------------------------

SELECT
    app_platform
    ,case
    when ua_id in ('UA-86733131-1', 'UA-86733131-16', 'UA-86733131-4', 'UA-86733131-11', 'UA-86733131-19') then 'Web'
    when ua_id in ('UA-86733131-2', 'UA-86733131-15', 'UA-86733131-5', 'UA-86733131-12', 'UA-86733131-18') then 'iOS'
    when ua_id in ('UA-86733131-3', 'UA-86733131-14', 'UA-86733131-6', 'UA-86733131-13', 'UA-86733131-17')  then 'Android'
    else 'Others'
    end  as app_os
    ,case 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  
        and regexp_match(upper(screen_name),'SPORT|SOCCER|TSS|PRE-GAME|EXCLUSIVE|RECENT MATCHES')
        and not regexp_match(upper(screen_name),'MOVIE|SERIES')
        or screen_name = ' - Article'
        then 'Sport' 
        
        when (ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and (regexp_match(upper(screen_name),'MOVIE|TV|SERIES|หนัง') 
        or regexp_match(upper(screen_name) , 'THEATER|ANIMATION|AYOTHAYA') ))
        or ua_id in ('UA-86733131-14','UA-86733131-15','UA-86733131-16') 
        then 'Movie/TV' 
        
        when (ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and regexp_match(upper(screen_name),'MUSIC|SONG|PLAYLIST|HITS|ARTIST|ดนตรี|อัลบั้ม|เพลง|MINI PLAYER') )
        or ua_id in ('UA-86733131-4','UA-86733131-5','UA-86733131-6') 
        then 'Music' 
        
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and (regexp_match(upper(screen_name),'TRAVEL|FOOD|GAME|MEN|WOMEN|ENTERTAINMENT|DARA|NEWS|LISTENING|LONGDO|KIDS|CLIP') 
        and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME'))
        then 'LifeStyle' 

        when ua_id in ('UA-86733131-1', 'UA-86733131-2', 'UA-86733131-3')
        and regexp_match(upper(screen_name),'TRUECLOUD') 
        then 'TrueCloud'
        
        when (ua_id in ('UA-86733131-13','UA-86733131-12', 'UA-86733131-11')
        and regexp_match(event_cate, 'Payment|Buy Extra') and NOT REGEXP_MATCH(event_action, 'Cancel')) 
        or (ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') and regexp_match(upper(screen_name),'PAYMENT'))
        then 'Payment'

        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')
        and regexp_match(upper(screen_name), 'BANNER|ADS') and not regexp_match(upper(screen_name), 'MEMBER')
        then 'Ads'
        
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and (regexp_match(upper(screen_name) ,'LOGIN|MY ACCOUNT|SETTINGS|PROFILE|REGISTER')
        or regexp_match(upper(screen_name) ,'SIGNIN|FORGET|ACCOUNTS|SIGNUP') )
        then 'User settings'

        when ua_id in ('UA-86733131-17','UA-86733131-18', 'UA-86733131-19') 
        or (ua_id in ('UA-86733131-1', 'UA-86733131-2', 'UA-86733131-3') 
        and screen_name in ('Privilege - All','Privilege - Detail','Privilege - Detail - Article',
        'Privilege - Dining', 'Privilege - Enjoy','Privilege - Featured','Privilege - Shopping','Privilege Near Me - All','Privilege Near Me - Dining'
        ,'Privilege Near Me - Enjoy','Privilege Near Me - Group' ,'Profile - TrueYou Mastercard','Privilege Near Me - Shopping'
        ,'Privilege - More','Privilege - Near Me','Profile - TrueYou Mastercard Info','Profile - TrueYou Mastercard QRcode')
        )

        then 'Privilege'
        else 'Others' 
    end as shelf_name
  ,fullVisitorId
from
(
select 
    fullVisitorId
    ,screen_name
    ,app_platform
    ,event_cate
    ,event_action
    ,ua_id
    FROM [true-dmp:DM_INTERNAL.tr_main_tracking] 
    where NOT (REGEXP_MATCH(screen_name, 'developers') or REGEXP_MATCH(screen_name, 'localhost/') or REGEXP_MATCH(screen_name, 'dev.'))
    AND (_PARTITIONTIME >= TIMESTAMP(DATE_ADD('2017-10-31',-30,'DAY')) AND _PARTITIONTIME <= TIMESTAMP('2017-10-31'))
    and app_name in ('TrueMusic', 'TrueTV', 'TrueID', 'iService', 'TrueYou')
)

------------------------------------- Tableau Monthly -----------------------------------------------------

SELECT
    app_platform
    ,case
    when ua_id in ('UA-86733131-1', 'UA-86733131-16', 'UA-86733131-4', 'UA-86733131-11', 'UA-86733131-19') then 'Web'
    when ua_id in ('UA-86733131-2', 'UA-86733131-15', 'UA-86733131-5', 'UA-86733131-12', 'UA-86733131-18') then 'iOS'
    when ua_id in ('UA-86733131-3', 'UA-86733131-14', 'UA-86733131-6', 'UA-86733131-13', 'UA-86733131-17')  then 'Android'
    else 'Others'
    end  as app_os
    ,case 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  
        and regexp_match(upper(screen_name),'SPORT|SOCCER|TSS|PRE-GAME|EXCLUSIVE|RECENT MATCHES')
        and not regexp_match(upper(screen_name),'MOVIE|SERIES')
        or screen_name = ' - Article'
        then 'Sport' 
        
        when (ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and (regexp_match(upper(screen_name),'MOVIE|TV|SERIES|หนัง') 
        or regexp_match(upper(screen_name) , 'THEATER|ANIMATION|AYOTHAYA') ))
        or ua_id in ('UA-86733131-14','UA-86733131-15','UA-86733131-16') 
        then 'Movie/TV' 
        
        when (ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and regexp_match(upper(screen_name),'MUSIC|SONG|PLAYLIST|HITS|ARTIST|ดนตรี|อัลบั้ม|เพลง|MINI PLAYER') )
        or ua_id in ('UA-86733131-4','UA-86733131-5','UA-86733131-6') 
        then 'Music' 
        
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and (regexp_match(upper(screen_name),'TRAVEL|FOOD|GAME|MEN|WOMEN|ENTERTAINMENT|DARA|NEWS|LISTENING|LONGDO|KIDS|CLIP') 
        and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME'))
        then 'LifeStyle' 

        when ua_id in ('UA-86733131-1', 'UA-86733131-2', 'UA-86733131-3')
        and regexp_match(upper(screen_name),'TRUECLOUD') 
        then 'TrueCloud'
        
        when (ua_id in ('UA-86733131-13','UA-86733131-12', 'UA-86733131-11')
        and regexp_match(event_cate, 'Payment|Buy Extra') and NOT REGEXP_MATCH(event_action, 'Cancel')) 
        or (ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') and regexp_match(upper(screen_name),'PAYMENT'))
        then 'Payment'

        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')
        and regexp_match(upper(screen_name), 'BANNER|ADS') and not regexp_match(upper(screen_name), 'MEMBER')
        then 'Ads'
        
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and (regexp_match(upper(screen_name) ,'LOGIN|MY ACCOUNT|SETTINGS|PROFILE|REGISTER')
        or regexp_match(upper(screen_name) ,'SIGNIN|FORGET|ACCOUNTS|SIGNUP') )
        then 'User settings'

        when ua_id in ('UA-86733131-17','UA-86733131-18', 'UA-86733131-19') 
        or (ua_id in ('UA-86733131-1', 'UA-86733131-2', 'UA-86733131-3') 
        and screen_name in ('Privilege - All','Privilege - Detail','Privilege - Detail - Article',
        'Privilege - Dining', 'Privilege - Enjoy','Privilege - Featured','Privilege - Shopping','Privilege Near Me - All','Privilege Near Me - Dining'
        ,'Privilege Near Me - Enjoy','Privilege Near Me - Group' ,'Profile - TrueYou Mastercard','Privilege Near Me - Shopping'
        ,'Privilege - More','Privilege - Near Me','Profile - TrueYou Mastercard Info','Profile - TrueYou Mastercard QRcode')
        )

        then 'Privilege'
        else 'Others' 
    end as shelf_name
  ,fullVisitorId
from
(
select 
    fullVisitorId
    ,screen_name
    ,app_platform
    ,event_cate
    ,event_action
    ,ua_id
    FROM [true-dmp:DM_INTERNAL.tr_main_tracking] 
    where NOT (REGEXP_MATCH(screen_name, 'developers') or REGEXP_MATCH(screen_name, 'localhost/') or REGEXP_MATCH(screen_name, 'dev.'))
    AND (_PARTITIONTIME >= TIMESTAMP(DATE_ADD(<Parameters.Start Date>,-30,'DAY')) AND _PARTITIONTIME <= TIMESTAMP(<Parameters.Start Date>))
    and app_name in ('TrueMusic', 'TrueTV', 'TrueID', 'iService', 'TrueYou')    
)
SELECT  
day_id
,app_platform
,app_name
,fullVisitorId 
,device_id
,ssoid 
,visit_start_dt
,visit_start_time1 
,case
    when ua_id in ('UA-86733131-1', 'UA-86733131-16', 'UA-86733131-4', 'UA-86733131-11', 'UA-86733131-19') then 'Web'
    when ua_id in ('UA-86733131-2', 'UA-86733131-15', 'UA-86733131-5', 'UA-86733131-12', 'UA-86733131-18') then 'iOS'
    when ua_id in ('UA-86733131-3', 'UA-86733131-14', 'UA-86733131-6', 'UA-86733131-13', 'UA-86733131-17')  then 'Android'
    end  as app_os
,case 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
        and regexp_match(upper(screen_name),'SPORT|SOCCER|TSS|PRE-GAME|EXCLUSIVE|RECENT MATCHES')
        and not regexp_match(upper(screen_name),'MOVIE|SERIES') then 'Sport' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'MOVIE|TV|SERIES|หนัง') or 	ua_id in ('UA-86733131-14','UA-86733131-15','UA-86733131-16')  --Android/IOS/Website for TrueTV	
        then 'Movie/TV' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'MUSIC|SONG|PLAYLIST|HITS|ARTIST|ดนตรี|อัลบั้ม|เพลง') or ua_id in ('UA-86733131-4','UA-86733131-5','UA-86733131-6')  --Website/IOS/Andriod for TrueMusic		
		then 'Music' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'TRAVEL|FOOD|GAME|MEN|WOMEN|ENTERTAINMENT|DARA|NEWS|LISTENING|LONGDO')
		and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
		then 'LifeStyle' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'TRUECLOUD')
		then 'TrueCloud'	
	when screen_name in ('Privilege - All','Privilege - Detail','Privilege - Detail - Article','Privilege - Dining','Privilege - Enjoy'
	,'Privilege - Featured','Privilege - Shopping','Privilege Near Me - All','Privilege Near Me - Dining','Privilege Near Me - Enjoy','Privilege Near Me - Group'
	,'Profile - TrueYou Mastercard','Privilege Near Me - Shopping','Privilege - More','Privilege - Near Me','Profile - TrueYou Mastercard Info','Profile - TrueYou Mastercard QRcode') then 'Privilege'
	else 'Others' end as shelf_name
,case 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'SPORT') then 'Sport' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'SOCCER|TSS|PRE-GAME|EXCLUSIVE|RECENT MATCHES')
        and not regexp_match(upper(screen_name),'MOVIE|SERIES')
		then 'Soccer' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'MOVIE|SERIES|หนัง')
		or 	ua_id in ('UA-86733131-14','UA-86733131-15','UA-86733131-16')  --Android/IOS/Website for TrueTV		
		then 'Movie' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'TV')
		or ua_id in ('UA-86733131-14','UA-86733131-15','UA-86733131-16')  --Android/IOS/Website for TrueTV		
		then 'TV' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'MUSIC|SONG|PLAYLIST|HITS|ARTIST|LISTENING|ดนตรี|อัลบั้ม|เพลง')
		or 	ua_id in ('UA-86733131-4','UA-86733131-5','UA-86733131-6')  --Website/IOS/Andriod for TrueMusic		
		then 'Music' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'GAME')
		and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
		then 'Game' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'TRAVEL') --and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
		then 'Travel'
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'FOOD') --and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
		then 'Food'
    when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'WOMEN') --and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
		then 'Women'
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'ENTERTAINMENT|DARA|NEWS') --and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
		then 'Entertainment' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'MEN')
		and not regexp_match(upper(screen_name),'WOMAN')
        and not regexp_match(upper(screen_name),'ENTERTAINMENT')
        and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
		then 'Men' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'LONGDO')
		then 'Longdo'
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'TRUECLOUD')
		then 'TrueCloud'
	when screen_name in ('Privilege - All','Privilege - Detail','Privilege - Detail - Article','Privilege - Dining','Privilege - Enjoy'
	,'Privilege - Featured','Privilege - Shopping','Privilege Near Me - All','Privilege Near Me - Dining','Privilege Near Me - Enjoy','Privilege Near Me - Group'
	,'Profile - TrueYou Mastercard','Privilege Near Me - Shopping','Privilege - More','Privilege - Near Me','Profile - TrueYou Mastercard Info','Profile - TrueYou Mastercard QRcode') then 'Privilege'
	else 'Others' end as shelf_in_app
,event_cate
,event_action
,screen_name 
,ua_id
FROM [true-dmp:$name_dataset1.$name_table1]
WHERE 
_PARTITIONTIME = TIMESTAMP('$YEAR-$MONTH-$DAY') 
	and NOT (REGEXP_MATCH(screenName, 'developers') and REGEXP_MATCH(screenName, 'localhost/'))
    and screen_name is not null 

---------------------------------------------------- daily -------------------------------------------------------------

SELECT
visit_start_dt 
,app_platform
,app_os
,shelf_name
,no_unique_user
from (
SELECT
    visit_start_dt 
    ,app_platform
    ,case
    when ua_id in ('UA-86733131-1', 'UA-86733131-16', 'UA-86733131-4', 'UA-86733131-11', 'UA-86733131-19') then 'Web'
    when ua_id in ('UA-86733131-2', 'UA-86733131-15', 'UA-86733131-5', 'UA-86733131-12', 'UA-86733131-18') then 'iOS'
    when ua_id in ('UA-86733131-3', 'UA-86733131-14', 'UA-86733131-6', 'UA-86733131-13', 'UA-86733131-17')  then 'Android'
    else 'Others'
    end  as app_os
    ,case 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  
        and regexp_match(upper(screen_name),'SPORT|SOCCER|TSS|PRE-GAME|EXCLUSIVE|RECENT MATCHES')
        and not regexp_match(upper(screen_name),'MOVIE|SERIES') 
        then 'Sport' 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  
        and regexp_match(upper(screen_name),'MOVIE|TV|SERIES|หนัง') 
        or ua_id in ('UA-86733131-14','UA-86733131-15','UA-86733131-16') 
        then 'Movie/TV' 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3') 
        and regexp_match(upper(screen_name),'MUSIC|SONG|PLAYLIST|HITS|ARTIST|ดนตรี|อัลบั้ม|เพลง') 
        or ua_id in ('UA-86733131-4','UA-86733131-5','UA-86733131-6') 
        then 'Music' 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  
        and regexp_match(upper(screen_name),'TRAVEL|FOOD|GAME|MEN|WOMEN|ENTERTAINMENT|DARA|NEWS|LISTENING|LONGDO') 
        and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME') 
        then 'LifeStyle' 

        when ua_id in ('UA-86733131-1', 'UA-86733131-2', 'UA-86733131-3')
        and regexp_match(upper(screen_name),'TRUECLOUD') 
        then 'TrueCloud'

        when screen_name in ('Privilege - All','Privilege - Detail','Privilege - Detail - Article','Privilege - Dining','Privilege - Enjoy' 
        ,'Privilege - Featured','Privilege - Shopping','Privilege Near Me - All','Privilege Near Me - Dining'
        ,'Privilege Near Me - Enjoy','Privilege Near Me - Group' ,'Profile - TrueYou Mastercard','Privilege Near Me - Shopping'
        ,'Privilege - More','Privilege - Near Me','Profile - TrueYou Mastercard Info','Profile - TrueYou Mastercard QRcode') 
        then 'Privilege'
        else 'Others' 
        end as shelf_name
  ,exact_count_distinct(fullVisitorId) as no_unique_user
from
(
select 
    visit_start_dt 
    ,fullVisitorId
    ,screen_name
    ,app_platform
    ,ua_id
    FROM [true-dmp:DM_INTERNAL.tr_main_tracking] 
    where NOT (REGEXP_MATCH(screen_name, 'developers') or REGEXP_MATCH(screen_name, 'localhost/') or REGEXP_MATCH(screen_name, 'dev.'))
    AND (_PARTITIONTIME >= TIMESTAMP(DATE_ADD('2017-10-31',-30,'DAY')) AND _PARTITIONTIME <= TIMESTAMP('2017-10-31'))
    and ua_id in ('UA-86733131-1', 'UA-86733131-16', 'UA-86733131-4', 'UA-86733131-11', 'UA-86733131-19',
    'UA-86733131-2', 'UA-86733131-15', 'UA-86733131-5', 'UA-86733131-12', 'UA-86733131-18',
    'UA-86733131-3', 'UA-86733131-14', 'UA-86733131-6', 'UA-86733131-13', 'UA-86733131-17')
)
group by 1,2,3,4
)

------------------------------------------------------------------------------------------------------------------------------
-- Shelf Daily Active View

SELECT
    visit_start_time1
    ,fullVisitorId
    ,app_platform
    ,case 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
        and regexp_match(upper(screen_name),'SPORT|SOCCER|TSS|PRE-GAME|EXCLUSIVE|RECENT MATCHES')
        and not regexp_match(upper(screen_name),'MOVIE|SERIES') then 'Sport' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'MOVIE|TV|SERIES|หนัง') or 	ua_id in ('UA-86733131-14','UA-86733131-15','UA-86733131-16')  --Android/IOS/Website for TrueTV	
        then 'Movie/TV' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'MUSIC|SONG|PLAYLIST|HITS|ARTIST|ดนตรี|อัลบั้ม|เพลง') or ua_id in ('UA-86733131-4','UA-86733131-5','UA-86733131-6')  --Website/IOS/Andriod for TrueMusic		
		then 'Music' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'TRAVEL|FOOD|GAME|MEN|WOMEN|ENTERTAINMENT|DARA|NEWS|LISTENING|LONGDO')
		and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
		then 'LifeStyle' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'TRUECLOUD')
		then 'TrueCloud'	
	when screen_name in ('Privilege - All','Privilege - Detail','Privilege - Detail - Article','Privilege - Dining','Privilege - Enjoy'
	,'Privilege - Featured','Privilege - Shopping','Privilege Near Me - All','Privilege Near Me - Dining','Privilege Near Me - Enjoy','Privilege Near Me - Group'
	,'Profile - TrueYou Mastercard','Privilege Near Me - Shopping','Privilege - More','Privilege - Near Me','Profile - TrueYou Mastercard Info','Profile - TrueYou Mastercard QRcode') then 'Privilege'
	else 'Others' end as shelf_name
	
    ,case 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'SPORT') then 'Sport' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'SOCCER|TSS|PRE-GAME|EXCLUSIVE|RECENT MATCHES')
        and not regexp_match(upper(screen_name),'MOVIE|SERIES')
		then 'Soccer' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'MOVIE|SERIES|หนัง')
		or 	ua_id in ('UA-86733131-14','UA-86733131-15','UA-86733131-16')  --Android/IOS/Website for TrueTV		
		then 'Movie' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'TV')
		or ua_id in ('UA-86733131-14','UA-86733131-15','UA-86733131-16')  --Android/IOS/Website for TrueTV		
		then 'TV' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'MUSIC|SONG|PLAYLIST|HITS|ARTIST|LISTENING|ดนตรี|อัลบั้ม|เพลง')
		or 	ua_id in ('UA-86733131-4','UA-86733131-5','UA-86733131-6')  --Website/IOS/Andriod for TrueMusic		
		then 'Music' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'GAME')
		and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
		then 'Game' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'TRAVEL') --and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
		then 'Travel'
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'FOOD') --and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
		then 'Food'
    when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'WOMEN') --and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
		then 'Women'
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'ENTERTAINMENT|DARA|NEWS') --and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
		then 'Entertainment' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'MEN')
		and not regexp_match(upper(screen_name),'WOMAN')
        and not regexp_match(upper(screen_name),'ENTERTAINMENT')
        and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
		then 'Men' 
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'LONGDO')
		then 'Longdo'
	when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
		and regexp_match(upper(screen_name),'TRUECLOUD')
		then 'TrueCloud'
	when screen_name in ('Privilege - All','Privilege - Detail','Privilege - Detail - Article','Privilege - Dining','Privilege - Enjoy'
	,'Privilege - Featured','Privilege - Shopping','Privilege Near Me - All','Privilege Near Me - Dining','Privilege Near Me - Enjoy','Privilege Near Me - Group'
	,'Profile - TrueYou Mastercard','Privilege Near Me - Shopping','Privilege - More','Privilege - Near Me','Profile - TrueYou Mastercard Info','Profile - TrueYou Mastercard QRcode') then 'Privilege'
	else 'Others' end as shelf_in_app
from
(
	select 
    visit_start_time1
    ,fullVisitorId
    ,screen_name
    ,app_platform 
    ,ua_id
    FROM [true-dmp:DM_INTERNAL.tr_main_tracking] 
    where NOT (REGEXP_MATCH(screen_name, 'developers') or REGEXP_MATCH(screen_name, 'localhost/') or REGEXP_MATCH(screen_name, 'dev.'))
    AND (_PARTITIONTIME >= TIMESTAMP(DATE_ADD('2017-10-31',-30,'DAY')) AND _PARTITIONTIME <= TIMESTAMP('2017-10-31'))
)

------------------------------------------------------------------------------------------------------------------------------


SELECT
    cast(date(visit_start_time1) as date) as visit_start_time1
    ,app_platform
    ,case 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
              and regexp_match(upper(screen_name),'SPORT|SOCCER|TSS|PRE-GAME|EXCLUSIVE|RECENT MATCHES')
              and not regexp_match(upper(screen_name),'MOVIE|SERIES') then 'Sport' 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
          and regexp_match(upper(screen_name),'MOVIE|TV|SERIES|หนัง') or 	ua_id in ('UA-86733131-14','UA-86733131-15','UA-86733131-16')  --Android/IOS/Website for TrueTV	
              then 'Movie/TV' 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
          and regexp_match(upper(screen_name),'MUSIC|SONG|PLAYLIST|HITS|ARTIST|ดนตรี|อัลบั้ม|เพลง') or ua_id in ('UA-86733131-4','UA-86733131-5','UA-86733131-6')  --Website/IOS/Andriod for TrueMusic		
          then 'Music' 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
          and regexp_match(upper(screen_name),'TRAVEL|FOOD|GAME|MEN|WOMEN|ENTERTAINMENT|DARA|NEWS|LISTENING|LONGDO')
          and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
          then 'LifeStyle' 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
          and regexp_match(upper(screen_name),'TRUECLOUD')
          then 'TrueCloud'	
        when screen_name in ('Privilege - All','Privilege - Detail','Privilege - Detail - Article','Privilege - Dining','Privilege - Enjoy'
        ,'Privilege - Featured','Privilege - Shopping','Privilege Near Me - All','Privilege Near Me - Dining','Privilege Near Me - Enjoy','Privilege Near Me - Group'
        ,'Profile - TrueYou Mastercard','Privilege Near Me - Shopping','Privilege - More','Privilege - Near Me','Profile - TrueYou Mastercard Info','Profile - TrueYou Mastercard QRcode') then 'Privilege'
        else 'Others' end as shelf_name
    ,case 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
          and regexp_match(upper(screen_name),'SPORT') then 'Sport' 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
          and regexp_match(upper(screen_name),'SOCCER|TSS|PRE-GAME|EXCLUSIVE|RECENT MATCHES')
              and not regexp_match(upper(screen_name),'MOVIE|SERIES')
          then 'Soccer' 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
          and regexp_match(upper(screen_name),'MOVIE|SERIES|หนัง')
          or 	ua_id in ('UA-86733131-14','UA-86733131-15','UA-86733131-16')  --Android/IOS/Website for TrueTV		
          then 'Movie' 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
          and regexp_match(upper(screen_name),'TV')
          or ua_id in ('UA-86733131-14','UA-86733131-15','UA-86733131-16')  --Android/IOS/Website for TrueTV		
          then 'TV' 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
          and regexp_match(upper(screen_name),'MUSIC|SONG|PLAYLIST|HITS|ARTIST|LISTENING|ดนตรี|อัลบั้ม|เพลง')
          or 	ua_id in ('UA-86733131-4','UA-86733131-5','UA-86733131-6')  --Website/IOS/Andriod for TrueMusic		
          then 'Music' 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
          and regexp_match(upper(screen_name),'GAME')
          and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
          then 'Game' 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
          and regexp_match(upper(screen_name),'TRAVEL') --and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
          then 'Travel'
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
          and regexp_match(upper(screen_name),'FOOD') --and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
          then 'Food'
          when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
          and regexp_match(upper(screen_name),'WOMEN') --and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
          then 'Women'
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
          and regexp_match(upper(screen_name),'ENTERTAINMENT|DARA|NEWS') --and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
          then 'Entertainment' 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
          and regexp_match(upper(screen_name),'MEN')
          and not regexp_match(upper(screen_name),'WOMAN')
              and not regexp_match(upper(screen_name),'ENTERTAINMENT')
              and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
          then 'Men' 
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
          and regexp_match(upper(screen_name),'LONGDO')
          then 'Longdo'
        when ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
          and regexp_match(upper(screen_name),'TRUECLOUD')
          then 'TrueCloud'
        when screen_name in ('Privilege - All','Privilege - Detail','Privilege - Detail - Article','Privilege - Dining','Privilege - Enjoy'
        ,'Privilege - Featured','Privilege - Shopping','Privilege Near Me - All','Privilege Near Me - Dining','Privilege Near Me - Enjoy','Privilege Near Me - Group'
        ,'Profile - TrueYou Mastercard','Privilege Near Me - Shopping','Privilege - More','Privilege - Near Me','Profile - TrueYou Mastercard Info','Profile - TrueYou Mastercard QRcode') then 'Privilege'
        else 'Others' end as shelf_in_app
  ,exact_count_distinct(fullVisitorId) as no_unique_user
from
(
	select 
    visit_start_time1
    ,fullVisitorId
    ,screen_name
    ,app_platform 
    ,ua_id
    FROM [true-dmp:DM_INTERNAL.tr_main_tracking] 
    where NOT (REGEXP_MATCH(screen_name, 'developers') or REGEXP_MATCH(screen_name, 'localhost/') or REGEXP_MATCH(screen_name, 'dev.'))
    AND (_PARTITIONTIME >= TIMESTAMP(DATE_ADD('2017-10-31',-30,'DAY')) AND _PARTITIONTIME <= TIMESTAMP('2017-10-31'))
)
group by 1,2,3,4
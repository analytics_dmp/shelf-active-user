#!/bin/bash
set -e

####
# CREATE THE DATA FOR THE FIRST TIME 
#########

currentDate=$(echo `date -d "$1 0 days" "+%Y-%m-%d"`)
echo "the date we are looking at is the $currentDate"
DAY=$(date -d "$currentDate" '+%d')
MONTH=$(date -d "$currentDate" '+%m')
YEAR=$(date -d "$currentDate" '+%Y')
name_dataset1=DM_INTERNAL
name_table1=tr_main_tracking
name_dataset2=DM_INTERNAL
name_table2=tr_shelf

echo "insert into partition"
table="$name_dataset2.$name_table2$"$YEAR$MONTH$DAY 	
echo $currentDate
query="
SELECT  
day_id
,app_platform
,app_name
,fullVisitorId 
,device_id
,ssoid 
,visit_start_dt
,visit_start_time1 
,case 
	when 	ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
				and regexp_match(upper(screen_name),'SPORT|SOCCER|TSS|PRE-GAME|EXCLUSIVE|RECENT MATCHES')
        and not regexp_match(upper(screen_name),'MOVIE|SERIES')
		then 'Sport' 
	
	when 	ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
				and regexp_match(upper(screen_name),'MOVIE|TV')
		or 		ua_id in ('UA-86733131-14','UA-86733131-15','UA-86733131-16')  --Android/IOS/Website for TrueTV		
		then 'Movie/TV' 

	when 	ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
				and regexp_match(upper(screen_name),'MUSIC|SONG|PLAYLIST|HITS|ARTIST|ดนตรี|อัลบั้ม|เพลง')
		or 		ua_id in ('UA-86733131-4','UA-86733131-5','UA-86733131-6')  --Website/IOS/Andriod for TrueMusic		
		then 'Music' 

	when 	ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
				and regexp_match(upper(screen_name),'TRAVEL|FOOD|GAME|MEN|WOMEN|ENTERTAINMENT|DARA|NEWS|LISTENING')
				and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
		then 'LifeStyle' 

	when screen_name in ('Privilege - All','Privilege - Detail','Privilege - Detail - Article','Privilege - Dining','Privilege - Enjoy'
	,'Privilege - Featured','Privilege - Shopping','Privilege Near Me - All','Privilege Near Me - Dining','Privilege Near Me - Enjoy','Privilege Near Me - Group'
	,'Profile - TrueYou Mastercard','Profile - TrueYou Mastercard QRcode') then 'Privilege'

		else 'Others' end as shelf_name
    ,case 
	when 	ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
				and regexp_match(upper(screen_name),'SPORT') 		
		then 'Sport' 
		
	when 	ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
				and regexp_match(upper(screen_name),'SOCCER|TSS|PRE-GAME|EXCLUSIVE|RECENT MATCHES')
        and not regexp_match(upper(screen_name),'MOVIE|SERIES')
		then 'Soccer' 
	
	when 	ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
				and regexp_match(upper(screen_name),'MOVIE')
		or 		ua_id in ('UA-86733131-14','UA-86733131-15','UA-86733131-16')  --Android/IOS/Website for TrueTV		
		then 'Movie' 
	
	when 	ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
				and regexp_match(upper(screen_name),'TV')
		or 		ua_id in ('UA-86733131-14','UA-86733131-15','UA-86733131-16')  --Android/IOS/Website for TrueTV		
		then 'TV' 
		
	when 	ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
				and regexp_match(upper(screen_name),'MUSIC|SONG|PLAYLIST|HITS|ARTIST|LISTENING|ดนตรี|อัลบั้ม|เพลง')
		or 		ua_id in ('UA-86733131-4','UA-86733131-5','UA-86733131-6')  --Website/IOS/Andriod for TrueMusic		
		then 'Music' 

	when 	ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
				and regexp_match(upper(screen_name),'GAME')
				and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
		then 'Game' 

	when 	ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
				and regexp_match(upper(screen_name),'TRAVEL')
				--and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
		then 'Travel' 
	when 	ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
				and regexp_match(upper(screen_name),'FOOD')
				--and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
		then 'Food' 
  when 	ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
				and regexp_match(upper(screen_name),'WOMEN')
				--and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
		then 'Women' 

	when 	ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
				and regexp_match(upper(screen_name),'ENTERTAINMENT|DARA|NEWS')
				--and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
		then 'Entertainment' 
    
	when 	ua_id in ('UA-86733131-1','UA-86733131-2','UA-86733131-3')  --Website/IOS/Android for TrueID
				and regexp_match(upper(screen_name),'MEN')
				and not regexp_match(upper(screen_name),'WOMAN')
        and not regexp_match(upper(screen_name),'ENTERTAINMENT')
        and not regexp_match(upper(screen_name),'PAYMENT|PRE-GAME')
		then 'Men' 
	when screen_name in ('Privilege - All','Privilege - Detail','Privilege - Detail - Article','Privilege - Dining','Privilege - Enjoy'
	,'Privilege - Featured','Privilege - Shopping','Privilege Near Me - All','Privilege Near Me - Dining','Privilege Near Me - Enjoy','Privilege Near Me - Group'
	,'Profile - TrueYou Mastercard','Profile - TrueYou Mastercard QRcode') then 'Privilege'
  else 'Others' end as shelf_in_app	
,event_cate
,event_action
,screen_name 
,ua_id
FROM [true-dmp:$name_dataset1.$name_table1]
WHERE 
_PARTITIONTIME = TIMESTAMP('$YEAR-$MONTH-$DAY') 
	and screen_name not like 'developers%'
    and screen_name is not null 
"
echo "upload to the main table"
execute_query=$(bq query --quiet --allow_large_results --replace --noflatten_results --destination_table "$table" "$query")